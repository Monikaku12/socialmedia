import requests,pprint
import configparser
import json

next_page_url = "https://itunes.apple.com/in/rss/customerreviews/id=1162120691/json"
temp = requests.get(url=next_page_url)
data = temp.json()
# print(data)
y = {}
a = {}
a = {'rating': data['feed']['entry'][0]['im:rating']['label']}
z = {'comments': data['feed']['entry'][1]['content']['label']}
w = {'id': data['feed']['entry'][0]['id']['label']}
entries = data['feed']['entry'];
for entry_details in entries:
    # print(entry_details)
    if id not in w:
        entries = data
        print(data)
    else:    
        feed_data = {
            'shop_id': '5ca1d5f8f54b8b534f8769d3', 
            'rating': entry_details['im:rating']['label'],
            'rating_parameter':[
            {
            'field_id': 'primary_rating',
            'question_type': 'rating',
            'component_type': 'Itms-emoticon-rater',
            'rating': 5
            }],
            'comment_title': entry_details['title']['label'],
            'comments': entry_details['content']['label'],
            'user_info': {
               'name': entry_details['author']['name']['label'],
               'customer_id': entry_details['id']['label']
            },
            'tag_app_last_updated_on': data['feed']['updated']['label'],
            'tag_app_version': entry_details['im:version']['label'],
            'tag_channel': 'ios_channel',
            'tag_alternate_url':data['feed']['link'][0]['attributes']['href'],
            'tag_self_url': data['feed']['link'][1]['attributes']['href'],
            'tag_first_page_url': data['feed']['link'][2]['attributes']['href'],
            'tag_last_page_url': data['feed']['link'][3]['attributes']['href'],
            'tag_previous_review_list_url': data['feed']['link'][4]['attributes']['href'],
            'tag_next_review_list_url': data['feed']['link'][5]['attributes']['href'],
            #'tag_doc_last_updated_on' : new Date()
        }
        print(feed_data)

        config = configparser.ConfigParser()
        config.read('/home/monica/Desktop/ios_app_store_task/config.properties')
        pprint.pprint(config.get('DEFAULT', 'shop_id'))
        shop_id = config['DEFAULT']['shop_id']
        nnhn_app_store_reviews = config['DEFAULT']['nnhn_app_store_reviews']
        url = "https://staging.litmusworld.com/rateus/api/feedbacks/save_feedback_ratings_user_info"
        headers = {
          'Content-Type': 'application/json',
        }

        response = requests.post(url, headers=headers, data=json.dumps(feed_data))
        pprint.pprint(response)
        responseData = response.json()
        pprint.pprint(responseData)

limit = 50; 
length_of_entries = 0
has_next = 0
while has_next == length_of_entries:
    try:
        temp_review = requests.get(url=next_page_url)
        data = temp_review.json()    
        y = {}
        a = {}
        a = {'rating': data['feed']['entry'][0]['im:rating']['label']}
        z = {'comments': data['feed']['entry'][1]['content']['label']}
        entries = data['feed']['entry'];
        length_of_entries = len(entries);   
        for entry_details in entries:
            feed_data = {
                'rating': entry_details['im:rating']['label'],
                'rating_parameter':[{
                    'field_id': 'primary_rating',
                    'question_type': 'rating',
                    'component_type': 'Itms-emoticon-rater',
                    'rating': entry_details['im:rating']['label']
                }],
                'comment_title': entry_details['title']['label'],
                'comments': entry_details['content']['label'],
                'user_info': {
                    'name': entry_details['author']['name']['label'],
                    'customer_id': entry_details['id']['label']
                },
                'tag_app_last_updated_on': data['feed']['updated']['label'],
                'tag_app_version': entry_details['im:version']['label'],
                'tag_channel': 'ios_channel',
                'tag_alternate_url':data['feed']['link'][0]['attributes']['href'],
                'tag_self_url': data['feed']['link'][1]['attributes']['href'],
                'tag_first_page_url': data['feed']['link'][2]['attributes']['href'],
                'tag_last_page_url': data['feed']['link'][3]['attributes']['href'],
                'tag_previous_review_list_url': data['feed']['link'][4]['attributes']['href'],
                'tag_next_review_list_url': data['feed']['link'][5]['attributes']['href']
                }
            print(feed_data)
    except KeyError as error:
        print('end page')             
if(limit == length_of_entries):
    next_page_url = data['feed']['link'][5]['attributes']['href']
    has_next = True 
    print(next_page_url)         
else:
    has_next = False
    pprint.pprint(data)
