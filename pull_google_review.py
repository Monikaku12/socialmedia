import requests,pprint
import configparser
import json

config = configparser.ConfigParser()
config.read('/home/monica/Desktop/google_task/configuration.properties')

last_modified=int(config['User-Info']['lastModified'])
print ("updated ' + str(last_modified) + ' seconds ago")
#print(last_modified)
#generate token
refresh_token=config['DEFAULT']['refresh_token']
client_id=config['DEFAULT']['client_id']
client_secret =config['DEFAULT']['client_secret']
app_id=config['DEFAULT']['app_id']
shop_id=config['DEFAULT']['shop_id']

maxResults = config['DEFAULT']['maxResults']
URL = "https://accounts.google.com/o/oauth2/token?refresh_token="+refresh_token+"&client_id="+client_id+"&client_secret="+client_secret+"&grant_type=refresh_token"
refresh_token_response = requests.post(url=URL)
#print(refresh_token_response.text)
data = refresh_token_response.json()
access_token = data['access_token']
url2 = config['DEFAULT']['url']
headers = {
        'Content-Type': 'application/json', 
    }   
#getting google reviews  
try:  
    URL = "https://www.googleapis.com/androidpublisher/v2/applications/"+app_id+"/reviews?access_token="+str(access_token)+"&maxResults="+maxResults
    temp = requests.get(url=URL)
    data_review = temp.json()
    pprint.pprint(data_review)

    y = {}
    a = {}
    #y = x[0]
    a = {'authorName': y['authorName']}
    a['reviewId'] = y['reviewId']
    z = y['comments']
    for r in z:
        p = {'comments':r}
        a.update(p['comments'])
    reviews_dict = {'review': {
    'shop_id': shop_id,
    'rating': a['userComment']['starRating'],
    'rating_parameter':[{
        'field_id':'primary_rating',
        'question_type':'rating',
        'component_type':'Itms-emoticon-rater',
        'rating': a['userComment']['starRating']
    }],
    'comments':a['userComment']['text'],
    'user_info':{
        'name': a['authorName'],
        'customer_id': a['reviewId']
    },
    'tag_review_id': a['reviewId'],
    'tag_review_id': a['reviewId'],
    'tag_device': a['userComment']['device'],
    'tag_android_os_version': a['userComment']['androidOsVersion'],
    'tag_app_version_code': a['userComment']['appVersionCode'],
    'tag_app_version_name': a['userComment']['appVersionName'],
    'tag_likes': a['userComment']['thumbsUpCount'],
    'tag_dislikes': a['userComment']['thumbsDownCount'],
    'tag_product_name': a['userComment']['deviceMetadata']['productName']

    }}
    if 'developerComment' in a:
        reviews_dict['review'].update({'developerComment': a['developerComment']['text']})  
    #pprint.pprint(reviews_dict)

    data = reviews_dict['review']

    last_modified_response_temp=a['userComment']['lastModified']['seconds']
    last_modified_response=int(a['userComment']['lastModified']['seconds'])
    print("new fetched reviews")
    print("review updated '+str(last_modified_response)+' seconds ago")
    if last_modified < last_modified_response:
        print("new reviews are added")
        response = requests.post(url=url2,headers=headers, data=json.dumps(data))
        response_j = response.json()
        pprint.pprint(response_j)
        print("review posted")

#get reviews for next page
        while "tokenPagination" in data_review:
            next_page_token = data_review["tokenPagination"]["nextPageToken"]
            del data_review["tokenPagination"]
            next_page_request = URL+"&token="+next_page_token	
            temp2_data_review = requests.get(url=next_page_request)
            data_review = temp2_data_review.json()

        #filtering reviews fields
            try:
                x = data_review['reviews']
                y = {}
                a = {}
                y = x[0]
                a = {'authorName': y['authorName']}
                a['reviewId']= y['reviewId']
                z = y['comments']
                for r in z:
                    p = {'comments':r}
                    a.update(p['comments'])
                    reviews_dict = {'review': {
                    'shop_id': shop_id,
                    'rating': a['userComment']['starRating'],
                    'rating_parameter':[{
                    'field_id':'primary_rating',
                    'question_type':'rating',
                    'component_type':'Itms-emoticon-rater',
                    'rating': a['userComment']['starRating']
                    }],
                    'comments':a['userComment']['text'],
                        'user_info':{
                        'name': a['authorName'],
                        'customer_id': a['reviewId']
                    },
                'tag_review_id': a['reviewId'],
                'tag_review_id': a['reviewId'],
                'tag_device': a['userComment']['device'],
                'tag_android_os_version': a['userComment']['androidOsVersion'],
                'tag_app_version_code': a['userComment']['appVersionCode'],
                'tag_app_version_name': a['userComment']['appVersionName'],
                'tag_thumps_up_count': a['userComment']['thumbsUpCount'],
                'tag_thumps_down_count': a['userComment']['thumbsDownCount'],
                'tag_product_name': a['userComment']['deviceMetadata']['productName']
                }}
                if 'developerComment' in a:
                    reviews_dict['review'].update({'developerComment': a['developerComment']['text']})	
                print("next page---------------------------------------------------------------------------------------------------------------")
                data = reviews_dict['review']

                last_modified_response=int(a['userComment']['lastModified']['seconds'])
                print("new fetched reviews")
                print("review updated '+str(last_modified_response)+' seconds ago")
                if last_modified < last_modified_response:
                    print("new reviews are added")
                    response = requests.post(url=url2,headers=headers, data=json.dumps(data))
                    #print(response.text)
                    response_j = response.json()
                    pprint.pprint(response_j)
                    print("review posted")
                else:
                    print("Review already exists")
            except KeyError as error:
                print("End of page")

        config.set('User-Info', 'lastModified', last_modified_response_temp)
        with open('/home/monica/Desktop/google_task/configuration.properties','w') as configfile:
            config.write(configfile)
        print("new reviews are added to the config")
except KeyError as error:
    print("No new review found")